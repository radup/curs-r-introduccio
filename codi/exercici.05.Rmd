---
title: "Exercici de la Sessió 5"
subtitle: "Curs R (OMD)"
author: "Xavier de Pedro"
date: "4 d'Abril de 2019 (generat el `r format(Sys.time(), '%d %B %Y')`)"
output: 
  slidy_presentation:
    footer: "Copyleft 2019, OMD-AjBCN. http://omd-gid.imi.bcn/CursR"
    number_sections: TRUE
    includes:
      in_header: apunts_capcalera.html
      after_body: apunts_peu.html
    css: styles.css 
tags:
- Exercici
- Curs
- R
- Formació
always_allow_html: yes
---
  
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Tasques

1. Omplir els forats que hi hagi als apunts i a l'arxiu de l'exemple amb el dataset `b` per a aquesta sessió 5 (`exemple.b.05.Rmd`) en què es demana escriure el codi R corresponent. Emprar sintaxi del `Tidyverse`, o alternativament, sintaxi de l'`R base`. 

---

# Tasques (ii)

2. **Resoldre el següent cas**:
Ens donen dins un arxiu d'Excel un joc de dades de signants d'una iniciativa ciutadana per **demanar l'alliberament dels Peixos Pacífics** (campanya del _llaç blau_). 
Heu de validar que aquestes persones són empadronades a la nostra ciutat al districte 1 del qual provenen les dades: **Quantes signatures són valides?** 
    * **Dades dels signants (and dni i/o nie)**: 
        * Carpeta `./dades/d/` > `signants.xls` > Full " _Signants_ "
    * **Dades del padró (amb nif_nie i codi de vivenda)**: 
        * Carpeta `./dades/d/` > `signants.xls` > Full " _Padró_ "
    * **Dades de les vivendes (amb codi vivenda i districte)**: 
        * Carpeta `./dades/d/` > `signants.xls` > Full " _Vivendes_ "

---

# Respondre a aquestes preguntes{.smaller}

1. Amb quina funció del paquet `dplyr` relacionada amb les unions de taules per clau comú (**join**) podem obtenir un data.frame amb les files del dataframe A que **NO** tenen la nostra clau de cerca al dataframe B? 

2. Quin és el resultat de 

```{r eval=FALSE}
q <- data.frame(v1 = c(1, NA, NA),
                v2 = c(NA, NA, NA),
                v3 = c(NA, NA, "b"))
q %>%
  remove_empty(c("rows", "cols"))

dim(q)
```

3. Quin és el resultat de:    

```{r eval=FALSE}
nums <- c(0.4, 0.5, 0.6, 0,7)
round(nums)
```

4. Quina funció (i de quin paquet) alternativa a la funció `round()` s'ha indicat en aquesta sessió per solucionar el problema d'arrodoniment aquí mostrat? 

