---
title: "Exercici de la Sessió 2"
subtitle: "Curs R (OMD)"
author: "Xavier de Pedro"
date: "12 de Març de 2019 (generat el `r format(Sys.time(), '%d %B %Y')`)"
output: 
  slidy_presentation:
    footer: "Copyleft 2019, OMD-AjBCN. http://omd-gid.imi.bcn/CursR"
    number_sections: TRUE
    includes:
      in_header: apunts_capcalera.html
      after_body: apunts_peu.html
    css: styles.css 
tags:
- Exercici
- Curs
- R
- Formació
always_allow_html: yes
---
  
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Tasques

1. Cerca per a què serveix la funció **rnorm** sense recòrrer a cercadors d'internet com google o similars sinó emprant només les ajudes integrades a través de la consola d'R o el programa RStudio.
1. fes una subselecció de dades del data.frame `dades.new`  per als registres amb edat igual o superior a 86 anys, i les columnes 1, 5 i 7
1. obté l'estadística descriptiva bàsica de les variables del data.frame `dades.new` amb la funció **summary**
1. fes un gràfic de dispersió amb la comanda **plot** de les variables `trimestre` i `edat` del data frame `dades.new`

# Respondre a aquestes preguntes

```{r}
# Dur les respostes a l'inici de la segûent sessió
```

1. Quina diferència hi ha entre carregar paquets amb `library()` i amb `p_load()` de la llibreria **pacman**?

2. Quina diferència hi ha entre un **data.frame** i un **vector**?

3. Quina diferència hi ha entre un **data.frame** i una **llista**?

4. Quina diferència hi ha entre un **data.frame** i una **matriu**?

5. Quin és el resultat de:    

```{r eval=FALSE}
trimestre <- seq(as.Date("2016/01/01"), as.Date("2019/01/01"), by = "quarter")[1:3]
trimestre[3]
```

