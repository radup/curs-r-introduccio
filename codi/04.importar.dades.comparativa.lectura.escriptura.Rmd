---
title: "Compara Lectura i Escriptura amb R"
subtitle: "En funció dels diferents paquets d'R emprats, sistemes de compressió i tipus de disc on escriure (SSD o IDE)"
institute: 'Departament de Gestió i Integració de Dades (GID). Oficina Municipal de Dades (OMD). Ajuntament de Barcelona'
author:
- Xavier de Pedro Puente^[Tècnic. GID - OMD - Aj.BCN]
date: "`r format(Sys.time(), '%d %B %Y')`"
output:
  html_document:
    df_print: paged
    toc: yes
    toc_float: yes
    number_sections: yes
    code_folding: show
    keep_md: false
tags:
- R
- Comparativa
- Lectura
- Escriptura
- ADUP
- OMD
abstract: |
  Veure codi a:
  https://gitlab.com/radup/curs-r-introduccio`
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, message =FALSE)

# Set overall timer to zero
start_all <- Sys.time()
```

# Inicialització

```{r Carrega Paquets (després d`instal·lar els que manquin), echo=F}
# Check Availability of package manager "PacMan" (and install it, if missing)
# if you have R < 3.5
if (version$major==3 && version$minor < 5) {
  # Instal.la devtools si et cal
  if (!require("devtools")) install.packages("devtools"); require("devtools")
  if (!require("pacman")) {
    cat("R Version: ", paste0(version$major,".",version$minor), ". ")
    cat("Tens una versió de R anterior a 3.5, per tant, instal.lem la versió corresponent de PacMan (0.4.1)\n")
    # Instal.la la darrera versió de pacman (0.4.1) que anava abans de la que va només amb R 3.5+ (pacman v0.5)
    install_version("pacman", version = "0.4.1");
    }
  require("pacman")
} else {
  # if you have R 3.5 or higher
  if (!require("pacman")) install.packages("pacman"); require("pacman")
}
p_load(rio)
p_load(tidyverse)
p_load(readxl)
p_load(writexl)
p_load(microbenchmark)
p_load(data.table)
p_load(feather)
p_load(fst)
p_load(R.utils)
p_load(sparklyr)
#p_load(ff, ffbase) # See also http://www.bnosac.be/index.php/blog/7-if-you-are-into-large-data-and-work-a-lot-package-ff
p_load(vroom)


# You need Java 8 for spark to use a supported java version
# > sudo apt install java-common openjdk-8-*
# > sudo R CMD javareconf
# Set java 8 as default javam with command:
# > sudo update-alternatives --config java

# install and configure spark -----------------------------------------------------------
#https://spark.rstudio.com/reference/spark_read_csv/
#spark_install()
#sc <- spark_connect(master="local")

```

```{r Defineix rutes, echo=F}
ruta.dades <- file.path("../dades", "c"); dir.create(ruta.dades, recursive=T, showWarnings=F)
ruta.logs <- "../logs"; dir.create(ruta.logs, recursive=T, showWarnings=F)
ruta.grafics <- "../grafics"; dir.create(ruta.grafics, recursive=T, showWarnings=F)

f.rel.gz <- list.files(path=ruta.dades, pattern="gz")
if (!file.exists(file.path(ruta.dades, "arxiu.gz"))) {
  file.link(file.path(ruta.dades, f.rel.gz), file.path(ruta.dades, "arxiu.gz")) 
}
if (!file.exists(file.path(ruta.dades, "arxiu"))) {
  gunzip(file.path(ruta.dades, "arxiu.gz"), remove=F)
}
f.abs.gz <- file.path(ruta.dades, f.rel.gz)
f.abs.gz[2] <- file.path(ruta.dades, "arxiu.gz")
f.abs <- gsub(".gz", "", f.abs.gz)

if (!file.exists(file.path(ruta.dades, "arxiu.csv"))) {
  # Crea els arxius a la carpeta del codi a partir de l'arxiu csv comprimit amb .gz a la carpeta dades/c/
  df <- read.csv(file.path(ruta.dades, "arxiu.gz"))
  write.csv(df, file.path(ruta.dades, "arxiu.csv"))
  write_fst(df, file.path(ruta.dades, "arxiu.50.fst"))
  write_fst(df, file.path(ruta.dades, "arxiu.100.fst"), compress=100)
  writexl::write_xlsx(list(Full1=df), file.path(ruta.dades, "arxiu.xlsx"))
  feather::write_feather(df, file.path(ruta.dades, "arxiu.feather"))
  gzip(file.path(ruta.dades, "arxiu.feather"), remove=F)
  saveRDS(df, file=paste0(f.abs[2], ".rds"))
  save(df, file=paste0(f.abs[2], ".RData"))
}
```

# Comparatives

## Lectura d'arxius
```{r comparativa Lectura d`arxius, eval=T, echo=T}
read.mbm = microbenchmark(
  base = read.csv(f.abs[2]),
  datatable = fread(f.abs[2]),
  datatable.df = as.data.frame(fread(f.abs[2])),
  tidyverse = read_csv(f.abs[2]),
  feather = read_feather(paste0(f.abs[2], ".feather")),
  vroom = vroom(f.abs[2]),
  xlsx_readxl = read_excel(paste0(f.abs[2], ".xlsx")),
  core.rds = readRDS(file=paste0(f.abs[2], ".rds")),
  core.RData = load(file=paste0(f.abs[2], ".RData")),
  base.gz = read.csv(f.abs.gz[2]),
  datatable.gz = fread(paste0("gunzip -c ", f.abs.gz[2])),
  tidyverse.gz = read_csv(f.abs.gz[2]),
  feather.gz = {
    gunzip(paste0(f.abs[2], ".feather.gz"), overwrite=T, remove=F)
    read_feather(paste0(f.abs[2], ".feather"))
    },
  vroom.gz = vroom(f.abs.gz[2]),
  fst.50 = read.fst(paste0(f.abs[2], ".50.fst")),
  fst.100 = read.fst(paste0(f.abs[2], ".100.fst")),
 # ff = read.csv.ffdf(file=paste0(getwd(), "/", f.abs[2]), header=TRUE),
 # sparklyr = spark_read_csv(sc, df, path=paste0("file://", getwd(), "/", f.abs[2])),
 # read_csvy() and write_csvy()
  times=3
)

read.mbm
```

```{r mostra gràfic comparativa lectures, eval=T, echo=T}
ggplot2::autoplot(read.mbm)
```

![Comparativa de lectura d'arxius sobre disc SSD](../grafics/compara_lectura_ssd.png)

------

![Comparativa de lectura d'arxius sobre disc IDE](../grafics/compara_lectura_ide.png)



```{r feinetes temporals, echo=F}
#d1 <- read_csv(f.abs.gz[2])
#d1.ff <- read.csv.ffdf(file=paste0(getwd(), "/", f.abs[2]), header=TRUE)
#d1b <- as.data.frame(d1.ff)
#setdiff(df, dfb)
#write_xlsx(df, paste0(f.abs[2], ".xlsx"))
#ffsave(df.ff, file="arxiu", rootpath="./dades")
#write_feather(df, paste0(f.abs[2], ".feather"))
#df.feather <- read_feather(paste0(f.abs[2], ".feather"))

# Test Spark Local --------------------------------------------------------
if (FALSE) {
  # Test reading csv into Spark  with Sparklyr
  spark_read_csv(sc, df, path=paste0("file://", getwd(), "/", f.abs[2]),
                 header = TRUE, columns = NULL, infer_schema = TRUE, 
                 delimiter = ",", quote = "\"", escape = "\\", 
                 charset = "UTF-8", null_value = NULL, options = list(), 
                 repartition = 0, memory = TRUE, overwrite = TRUE)
}

```
## Escriptura d'arxius
```{r comparativa escriptura d`arxius, eval=T, echo=T}
write.mbm = microbenchmark(
  base = write.csv(df, paste0(f.abs[2], ".base.csv")),
  tidyverse = write_csv(df, paste0(f.abs[2], ".tidyverse.csv")),
  datatable = fwrite(df, paste0(f.abs[2], ".datatable.csv")),
  core.rds = saveRDS(df, file=paste0(f.abs[2], ".rds")),
  core.RData = save(df, file=paste0(f.abs[2], ".RData")),
  feather = write_feather(df, paste0(f.abs[2], ".feather")),
  vroom = vroom_write(df, paste0(f.abs[2], ".vroom.csv")),
  fst.50 = write.fst(df, paste0(f.abs[2], ".50.fst")),
  fst.100 = write.fst(df, paste0(f.abs[2], ".100.fst"), 100),
  base.gz = write.csv(df, gzfile(paste0(f.abs[2], ".base.csv.gz"))),
  tidyverse.gz = write_csv(df, gzfile(paste0(f.abs[2], ".tidyverse.csv.gz"))),
  datatable.gz = {
    fwrite(df, paste0(f.abs[2], ".datatable.csv"))
    gzip(paste0(f.abs[2], ".datatable.csv"), overwrite=T, remove=F)
  },
  feather.gz = {
    write_feather(df, paste0(f.abs[2], ".feather"))
    gzip(paste0(f.abs[2], ".feather"), overwrite=T, remove=F)
  },
  vroom.gz = vroom_write(df, paste0(f.abs[2], ".vroom.csv.gz")),
  vroom.pigz.gz = vroom_write(df, pipe(paste0("pigz > ", paste0(f.abs[2], ".vroom.csv.pigz.gz")))),
  fst.50.gz = {
    write.fst(df, paste0(f.abs[2], ".50.fst"))
    gzip(paste0(f.abs[2], ".50.fst"), overwrite=T, remove=F)
    },
  fst.100.gz = {
    write.fst(df, paste0(f.abs[2], ".100.fst"), 100)
    gzip(paste0(f.abs[2], ".100.fst"), overwrite=T, remove=F)
    },
  xlsx_writexl = write_xlsx(df, paste0(f.abs[2], ".xlsx")),
  #ff = write.csv.ffdf(df.ff, file=paste0(getwd(), "/", f.abs[2], ".ff")),
  #  # read_csvy() and write_csvy()
  times=3
)

write.mbm
```


```{r mostra gràfic comparativa escriptures, eval=T, echo=T}
ggplot2::autoplot(write.mbm)
```
![Comparativa d'escriptura d'arxius en disc SSD](../grafics/compara_escriptura_ssd.png)

******

![Comparativa d'escriptura d'arxius en disc IDE](../grafics/compara_escriptura_ide.png)

# Finalització

```{r desa informació de la sessió i paquets emprats, echo=F}
sink(file.path(ruta.logs,
               paste0("log_", 
                      format(Sys.Date(), format="%y%m%d"), 
                      ".txt")
               )
     )
cat("Sys.info() : \n")
cat("--------------------\n")
data.frame(Sys.info())
cat("\n\nsessionInfo() : \n")
cat("--------------------\n")
sessionInfo()
cat("--------------------\n")
# Show overall duration of the run
duration_all <- Sys.time()-start_all;
duration_all_msg <- paste(duration_all, attr(duration_all, "units"), sep=" ");
cat(paste0("\nDurantion of the whole run: ", duration_all_msg));

sink()

```

