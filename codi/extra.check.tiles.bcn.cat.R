library(leaflet)
library(dplyr)
map.bcn.cat <- leaflet(options = leafletOptions(minZoom = 12, maxZoom = 18) ) %>%
  setView(2.177562187,41.380762500, zoom = 12) %>% # Posicionem el centre de la imatge a l'OMD
  setMaxBounds(1.952386669,41.281036877, 2.347955988,41.479131073) %>% # Establim límits màxims per a la navegació a sobre del mapa
  addMarkers(lng=2.177562187, lat=41.380762500,
             popup="<b>Oficina Municipal de Dades</b><br/><i>c. Avinyó, 32</i>") %>%
#  addTiles("https://tiles.bcn.cat/tiles/XYZ/GuiaGPKG/{z}/{x}/{y}.png",
  addTiles("https://tiles.bcn.cat/tiles/XYZ/Parceles/{z}/{x}/{y}.png",
           attribution = "Cartografia &copy; <a href='http://www.bcn.cat'>Ajuntament de Barcelona</a>")

map.bcn.cat

map.scrumworker <- leaflet(options = leafletOptions(minZoom = 12, maxZoom = 18) ) %>%
  setView(2.177562187,41.380762500, zoom = 12) %>% # Posicionem el centre de la imatge a l'OMD
  setMaxBounds(1.952386669,41.281036877, 2.347955988,41.479131073) %>% # Establim límits màxims per a la navegació a sobre del mapa
  addMarkers(lng=2.177562187, lat=41.380762500,
             popup="<b>Oficina Municipal de Dades</b><br/><i>c. Avinyó, 32</i>") %>%
  addTiles("https://omd-gid.ajuntament.bcn/tiles/XYZ/GuiaGPKG/{z}/{x}/{y}.png",
           attribution = "Cartografia &copy; <a href='http://www.bcn.cat'>Ajuntament de Barcelona</a>")

map.scrumworker
