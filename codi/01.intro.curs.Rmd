---
title: "Curs d`R"
author: "Xavier de Pedro Puente  (OMD-AjBCN)"
date: "5 de Març 2019 (generat el `r format(Sys.time(), '%d %B %Y')`)"
output: 
  slidy_presentation:
    footer: "Copyleft 2019, OMD-AjBCN. http://omd-gid.imi.bcn/CursR"
    number_sections: TRUE
    includes:
      in_header: apunts_capcalera.html
      after_body: apunts_peu.html
    css: styles.css  
institute: Oficina Municipal de Dades (OMD). Ajuntament de Barcelona
subtitle: 'Sessió 1: Introducció, R, Rstudio. Panoràmica general'
always_allow_html: yes
---


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
if (!require("devtools")) install.packages("devtools"); require(devtools)
if (!require("pacman")) install_version("pacman", version = "0.4.1"); require(pacman)
p_load(dplyr)
p_load(kableExtra)
#  powerpoint_presentation:
#    reference_doc: my-styles.pptx
#    reference_doc: ppt_ajbcn_2017.pptx
#  slidy_presentation:
#    footer: "Copyleft (cc-by-sa) 2019, OMD"
#
# png de cheatsheets a:
# https://github.com/rstudio/cheatsheets/tree/master/pngs

programa <- t(data.frame(
  c("1", "5 Març", "Introducció, R, Rstudio. Panoràmica general"),
  c("2", "12 Març", "Objectes d'R i funcions bàsiques, paquets habituals"),
  c("3", "19 Març", "Introducció a R Modern amb Tidyverse. Anàlisi de dades - 1a part"),
  c("4", "26 Març", "Importar dades"),
  c("5", "2 Abril", "Anàlisi de dades - 2a part"),
  c("6", "9 Abril", "Anàlisi de dades - 3a part"),
  c("7", "23 Abril", "Taules: text, html, latex, pdf. Format condicional."),
  c("8", "30 Abril", "Gràfics i Mapes: png, svg, shp, interactius - 1a part"),
  c("9", "7 Maig", "Gràfics i Mapes: png, svg, shp, interactius - 2a part"),
  c("10", "14 Maig", "Compartir codi de forma sostenible amb Git i Gitlab")
))
rownames(programa) <- programa[,1]
colnames(programa) <- c("Sessió", "Data", "Temàtica")
programa <- data.frame(programa, row.names = NULL)
```

## Programa del Curs {data-background=#ff0000}

`r knitr::kable(programa) %>% kable_styling(bootstrap_options = c("striped", "hover"), font_size = 22)`

Programari principal a tenir instal·lat per aquest curs:

* R + RStudio + Git  ![](../grafics/logos_r_i_rstudio.png){ width=10% }  ![](../grafics/512px-Git-logo.svg.png){ width=7% } 

## Introducció a R

- Presentació del programari R.
- Instal·lació de R i Inici de la sessió.
- Sintaxi i programació bàsica en R.
- Paquets d'R i repositoris (CRAN, MRAN, Git, altres).
- Scripts (.R i .Rmd).
- Interfícies d'usuari (RStudio, Radiant, R Commander, altres)

## Presentació del programari R.

* R és un potent i flexible programari pensat per tractar dades, fer gràfics i anàlisis estadístics. 
* També és un llenguatge de programació amb funcions orientades a objectes.
* És programari lliure i funciona sota GNU/Linux, MAC OSX, MS Windows i altres.

### Ús de dades externes

R ofereix moltes opcions per carregar dades externes, inclosos els fitxers Excel, PDF, SPSS, o de text pla (txt, csv, tab, ...), així com fer bases de dades de tot tipus.

---

### Instal·lació R + RStudio + Git ![](../grafics/logos_r_i_rstudio.png){width=20%}  ![](../grafics/512px-Git-logo.svg.png){width=13%}  

* Instal.la R des d'un dels llocs mirall a http://cran.r-project.org/mirrors.html (o via IMI en cas Aj.BCN)
* Una vegada instal·lat R ja el podem utilitzar, el que passa és que és una mica espartà d'utilitzar com a tal, i per això fem servir un IDE (Entorn integrat de desenvolupament - en anlgès), que bàsicament ens proveeix d'una GUI (Interfície Gràfica d'Usuari - en anglès) per facilitar el treball amb R. 
* L'IDE més utilitzat per R és [RStudio](https://www.rstudio.com), i és el que nosaltres utilitzarem. Pots instal·lar-lo des de https://www.rstudio.com
* El programa Git ( https://gitforwindows.org/ o equivalent per al teu sistema operatiu ) ens permetrà compartir arxius i canvis dels arxius de forma segura i escalable/sostenible amb altres persones. Habitualment via Gitlab.com o Github.com, i en el nostre cas, via: https://gitlab.ajuntament.bcn

---

### USB Disc del Curs: Slax (GNU/Linux) Portable{.smaller}

El disc USB del curs té 2 particions:
```{r}
# ├── 7gbntfs                      <- Disc de dades estàndar visible des de Linux i Windows. 7 Gb
# │   └── curs-r-introduccio
# ├── 8gbext4                      <- Per inici amb Slax GNU/Linux (no visible des de Windows). 8 Gb
# │   ├── lost+found
# │   └── slax
```
Inici de GNU/Linux Slax Portable (escollint opció d'inici USB des de la **BIOS**):

![](../grafics/slax_ajbcn_cursR_00_escriptori.png){ width=25% } ![](../grafics/slax_ajbcn_cursR_00_menu_progs.png){ width=25% } ![](../grafics/slax_sjbcn_cursR_01_RStudio_exemple.png){ width=25% } 

**Accés a la BIOS?** Habitualment: prémer `F8` ó `F12` ó `Enter` ó `Esc` en encendre l'ordinador, per accedir a la BIOS o poder escollir l'inici per disc USB. 

---

### Connexió Wifi Durant el Curs?

![](../grafics/slaxusb_connexio_wifi_ajbcn.png){ width=70% }

---

### Sessió R

Després d'iniciar R, hi ha una consola que espera l'entrada. A l'indicatiu (>), podeu introduir números i fer càlculs.
```{r}
1 + 2
```

![](../grafics/rstudio_panells_en_slaxusb_00.png){ width=45% } 

---

### Excel vs R

Pensem un moment en la diferència de com es treballar amb un Full de Càcul (**LibreOffice Calc**, **MS Excel** o altres) ...

...respecte a com es treballa amb llenguatges de programació, fent scripts d'instruccions (amb el llenguatge R ó altres)

Les accions de l'usuari, clics, tasques, instruccions... :

* es poden convertir en un idioma d'instruccions ("**Codi**") llegible per altres 
* són instruccions revisables per altres (es facilita la revisió per parells o iguals)
* són instruccions reproduïbles per un mateix o altres en altres contextos
* són instruccions reaprofitables per ser modificades i ampliades per a nous usos per un mateix o altres 
* ...

---

### Assignació variable

En molts llenguatges s'utilitza l'operador d'assignació "=" per assignar valors a variables:

```{r}
x = 1
x
```

En R, en canvi, se sol emprar la fletxa cap a l'esquerra per fer l'assignació: "<-".

```{r}
x <- 2
x
```

---

### Funcions

Les funcions R són invocades pel seu nom, després seguides pel parèntesi, i zero o més arguments. A continuació, s'aplica la funció c per combinar tres valors numèrics en un vector.

```{r}
c(1, 2, 3)
```

### Separador d'instruccions

Per separar instruccions dins una mateixa línia podem fer servir el ";"
```{r}
x <- c(1, 2, 3); y <- x*x
```

També podem posar les instruccions en línies separades.

---

### Comentaris
Tot el text després del signe de coixinet "#" dins de la mateixa línia es considera un comentari.

```{r}
1 + 1 # aquest és un comentari
```

---

### Afegint paquets{.smaller}
De vegades, necessitem una funcionalitat addicional més enllà de les ofertes per la biblioteca R bàsica. 

Per instal·lar un paquet d'extensió, hauríeu d'invocar la funció install.packages a la consola d'R i seguir-ne les instruccions. Per exemple, per al paquet DT:

```{r eval=FALSE}
install.packages("DT")
```
Un cop instal·lat l'haurem de carregar a la biblioteca:

```{r , eval=FALSE}
library(DT) # require(DT)
```
Si volem saber-ne més... :

```{r , eval=FALSE}
library(help="DT")
```
La comunitat R és qui elabora i manté aquests paquets (alguns signats per estadístics de gran renom).

---

### Carregant paquets (instal·lant-los si cal abans): pacman{.smaller}
Sovint posem la instrucció `install.packages("Foo")` al principi d'un script per que l'instal·li la primera vegada, abans de voler carregar-lo en memòria amb `library("Foo")`. Però això fa que s'instal.li cada vegada.

Hi ha diversos paquets d'R que ajuden a evitar això. Un d'ells és **pacman**, que és el que emprarem sovint aquí.

```{r eval=FALSE}
install.packages("pacman") # Per a R >= 3.5
devtools::install_version("pacman", version = "0.4.1") # Per a R < 3.5

# Un cop instal·lat pacman l'haurem de carregar a la biblioteca:
library(pacman) # require(pacman)

# I ara ja podem carregar nous paquets que necessitem amb:
p_load("formattable")
```
`p_load()` (respecte _install.packages()_ + _library()_ ): En cas que el paquet no el tinguem instal·lat, s'instal·larà primer, i després es carregarà en memòria. Si ja el teníem instal·lat, només es carregarà en memòria.

---

### Gràfics
Es poden crear de molts tipus, amb el "device" gràfic per omissió d'R o altres.
```{r out.width=c('50%', '50%'), fig.show='hold'}
x <- 1:10 # equival a x <- c(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
boxplot(x)
y <- x*x*x
plot(x, y)
```

---

### Trucs de Consola

* Si ens equivoquem podem prémer la tecla "ESC" i tornarà a aparèixer el símbol ">"
* Podem navegar per les instruccions que ja hem executat fent servir les tecles de les fletxes amunt i avall del teclat
* Per sortir del programa podem tancar o executar ">q()"

---

## Espai de treball

* És l'espai on es desen de forma temporal o no tots els objectes que anem creant o carregant.
* Quan tanquem el programa el podem desar.

```{r , eval=FALSE}
getwd() # on és el meu espai de treball
ls() # quins objectes hi ha?

setwd("PATH") # hem de substituir el "path" pel de l'espai de 
# treball on volem estar

save.image("Prova.Rdata")

```

---


### Obtenir ajuda: cerca exacta
Hi ha diferents maneres d'accedir a l'ajuda dins el programa:

```{r , eval=FALSE}
help.start()           # Inicialitzar l'ajuda

help.search("normal")  # Buscar "normal" a l'ajuda

help(package="base")   # Buscar info del paquet "base"

?log                # Buscar ajuda d'una funció concreta

```

---

### Obtenir ajuda: cerca difusa
Si no esteu segur del nom de la funció que esteu cercant, podeu realitzar una cerca difusa amb la funció apropos.

```{r}
apropos("nova")
```

Tingueu en compte que això només cerca entre els paquets de R carregats en l'entorn de treball en aquell moment

....
Finalment, hi ha un motor de cerca específic d'Internet a http://www.rseek.org per obtenir més assistència.

---

## Codificacions (encodings)
A vegades, sobre tot en Windows, quan obrim un fitxer no es veuen bé els accents.

* Això és a causa de les codificacions de caracters, és a dir, com el sistema interpreta els números i lletres.
    * Els dos més freqüents són UTF-8 (internacional) i iso-8859-1 (anomenat també Latin1, o windows-1252, l'habitual a Europa Occidental)
* En cas de que, en obrir un fitxer, no se us vegin bé els accents, podeu anar a:
    * File -> Reopen with encoding -> UTF-8.
* Si tot i així no es veu bé, poder fer:
    * File -> Reopen with encoding -> Show all encodings -> LATIN1.


## Sintaxi i programació bàsica en R.
<center>
![](https://raw.githubusercontent.com/rstudio/cheatsheets/master/pngs/base-r.png){width="60%"}
</center>
https://github.com/rstudio/cheatsheets/blob/master/base-r.pdf
   
## Altres conceptes

* Repositoris de Paquets d'R
    * CRAN: The Comprehensive R Archive Network
    * MRAN: The Microsoft R Archive Network (per a 'checkpoint')
    * Bioconductor: Repositori de Bioinformàtica
* Altres fonts de paquets i codi
    * Gitlab, Github, Bitbucket, ...
    * source("http://example.com/foo.R");source("F:/bar.R")
    * Packrat: gestió de paquets d'R per projecte
* Scripts d'R
    * .R
    * .Rmd (Rmd document, Rmd notebook)
* Interfícies d'usuari (IDE, GUI)
    * RStudio, Radiant, R Commander, altres.

## RStudio
- RStudio Cheatsheet: Panells, Entorn de Treball 
    - Entorn: Visualització de 'list' vs 'grid'
- RStudio addins (R package addinslist)
  - Addins destacats
    1. CRANsearcher (Search R packages in default Repository)
    1. RegExplain (Regular Expression Helper)
    1. citr (Insert citations)
    1. mapedit.addin (Draw to select regions on map via GUI)
    1. rhandsontable (edit Data Frame via GUI)
    1. ...
- I addicionalment
https://b-rodrigues.github.io/modern_R/getting-to-know-rstudio.html

---

### Panells de Rstudio
<center>
![](../grafics/rstudio_panells_en_slaxusb_00.png){ width=70% } 
</center>
---

<center>
![](https://github.com/rstudio/cheatsheets/raw/master/pngs/rstudio-ide.png){width="80%"}
https://resources.rstudio.com/rstudio-cheatsheets/rstudio-ide-cheat-sheet
</center>
---

### Cerca de paquets a Rstudio (i): per nom

Podem consultar la llista de paquets d'R disponibles a través del panell corresponent de RStudio.

### Cerca de paquets a Rstudio (ii): per nom i descripció, via Rstudio addin{.smaller}

Però això només cerca paquets pel nom del paquet, i sovint això no ajuda a descobrir nous paquets en que estem interessats per que no sabem el seu nom concret. 

Per a això han fet un cercador de paquets pel nom i descripció, com a RStudio Addin, que cerca al principal repositori de paquets (CRAN): CRANSearcher. 


<center>
![](../grafics/rstudio_addins_00_cran_searcher.png){ width=60% } 
</center>

---

### Cerca de paquets a Rstudio (iii): Rstudio addin CRANsearcher
<center>
![](../grafics/rstudio_addins_01_cran_searcher_window.png){ width=90% } 
</center>

---

## Altres GUI: Radiant (Shiny) 
Via Shiny UI
https://radiant-rstats.github.io/docs/install.html
https://github.com/radiant-rstats/radiant
<center>
![](https://radiant-rstats.github.io/docs/data/figures/expand_grid.png){width="80%"}
</center>

## Altres GUI: R Commander (Rcmdr) - per estadística
<center>
![](https://socialsciences.mcmaster.ca/jfox/Misc/Rcmdr/Rcmdr-screenshot.jpg){width="50%"}
</center>


## R tradicional (R core) vs R Modern (Tidyverse) {.smaller}
<center> 
![](https://raw.githubusercontent.com/rstudio/cheatsheets/master/pngs/syntax.png){width="65%"}
</center>
https://github.com/rstudio/cheatsheets/blob/master/syntax.pdf

---

## Paquets R del Tidyverse
![](https://rviews.rstudio.com/post/2017-06-09-What-is-the-tidyverse_files/tidyverse1.png){width="70%"}
Més informació:

- [TidyTools manifesto (Hadley Wickham)](https://cran.r-project.org/web/packages/tidyverse/vignettes/manifesto.html)
- https://rviews.rstudio.com/2017/06/08/what-is-the-tidyverse/
- i les sessions 3, 4 i 5 d'aquest curs.

---

## Combinar plantilles i codi
Hi ha diverses maneres: 

* Via **KnitR** (amb R Markdown)
* Via **Sweave** (amb latex)
* Via **odfWeave** (amb documents Open Document)
* Via **Smarty** (amb [Tiki](https://r.tiki.org) + [PluginR](https://doc.tiki.org/PluginR))

A OMD emprarem fonamentalment la via del KnitR amb plantilles en R Markdown.

## Més informació sobre R Markdown
La veurem a la sessió 2, però per als curiosos: 

- https://rmarkdown.rstudio.com/
- https://rmarkdown.rstudio.com/lesson-1.html (Video intro 1.5')
- https://rmarkdown.rstudio.com/lesson-2.html

Diapositiva breu sobre Rmd: https://github.com/rstudio/webinars/blob/master/02-Reproducible-Reporting/1-rmarkdown.Rmd

## On desem els nostres arxius durant el Curs{.smaller}

Podeu escollir entre a una unitat de xarxa (les del perfil d'usuari de l'Ajuntament de Barcelona), localment en usb, o online a llocs com Gitlab (s'explicarà a la sessió 10 d'aquest curs)

### Respecte el disc USB

L'USB del curs té 2 particions: 

(1) No és visible des de MS Windows (té sistema d'arxius **ext4**). Serveix per a l'arrencada del sistema operatiu Linux portable mini amb els programes del curs (R, Rstudio, Git, ...). 

    * Aquest sistema operatiu Linux portable està basat en una distribució **Slax 9.x**, basada a la seva vegada en **Debian Stretch**, de [GNU/Linux](https://es.wikipedia.org/wiki/GNU/Linux). 

(2) És visible des de Linux i Windows (amb sistema d'arxius **ntfs**). Està pensada per a deixar-hi els arxius del curs (apunts, exercicis, etc), per tal que hi pogueu accedir tant des de l'slax linux de l'usb com des del vostre ordiandor de la feina.

```{r}
# Podem prestar durant la durada del curs un exemplar d'aquest disc SlaxUSBdual a qui en necessiti 
# Agrairem que en acabar el curs en els torneu a la OMD de l'Ajuntament de Barcelona 
# (C/Avinyó 32, 2a planta. Barcelona)
```

### Respecte Gitlab...

Al de l'Ajuntament de Barcelona [https://gitlab.ajuntament.bcn](https://gitlab.ajuntament.bcn). O a gitlab.com [https://gitlab.com](https://gitlab.com)

## On formular preguntes entre sessions{.smaller}

* En un futur, potser al HumHub de l'Ajuntament:
    * Idealment s'haurien d'enviar en un espai comú com un fòrum (o equivalent) obert temàtic sobre R. S'ha demanat de crear un espai dins de la nova eina col·laborativa corporativa - **[HumHub](https://humhub.org)** de l'Ajuntament de Barcelona, malgrat no sembla obert (al menys actualment) als altres ents de la corporació de l'Ajuntament de Barcelona (BCN Activa, ASPB, ...). 
* Actualment, al Forum d'R de l'ADUP - https://adup.cat/forum2
    * Addicionalment s'ha creat un espai obert d'intercanvi de coneixement entre usuaris d'R de les administracions públiques i altres entitats sense finalitat de lucre, dins el portal "**ADUP - Analistes de Dades d'Utilitat Pública**". 
    * Aquest espai és obert a tothom qui vulgui, inclós BCN ACtiva, ASPB, ACA, ... 
    * **Suggereixo que escriviu allà els dubtes que tingueu entre les sessions presencials del Curs, per tal d'anar generant comunitat d'usuaris**:
* Alternativament, o en altres casos, per correu-e
    * Xavier de Pedro <**xdepedro (a) bcn.cat**>
    * Però aquest sistema no és el desitjable per que les respostes a uns no són llegibles pels altres, etc.

## Recursos (i)
* Codi:
    * GitLab AjBCN: https://gitlab.ajuntament.bcn
    * GitLab ADUP: https://gitlab.com/radup/
* Manuals Introductoris:
    * R para Principiantes
      https://cran.r-project.org/doc/contrib/rdebuts_es.pdf
* Llibres:
    * Data Science for R (pdf i online): 
      http://r4ds.had.co.nz
    * [Using R and RStudio for Data Management, Statistical Analysis, and Graphics, 2nd edition](https://www.amazon.com/Management-Statistical-Analysis-Graphics-2015-04-27/dp/B017POAKSK/ref=sr_1_5?ie=UTF8&qid=1539194380&sr=8-5&keywords=using+r+and+rstudio+for+data+management)

## Recursos (ii) 
* Cursos en-línia
    * Coursera's Data Science Specialization (MOOC) 
      https://github.com/DataScienceSpecialization/courses
    * “Intro to R for Journalists: How to Find Great Stories in Data” (MOOC)
      https://journalismcourses.org/RC0818.html
    * Llista de tutorials R i cursos: 
      https://hackr.io/tutorials/learn-r
    * Data Management Programming and Graphics with R:
    * https://uebvhir.github.io/R-Course.html

* Cursos presencials
    * Barcelona activa
      https://cibernarium.barcelonactiva.cat/ficha-actividad?activityId=873551

## Recursos (iii)
* Xuletes - Cheatsheets:
    * https://www.rstudio.com/resources/cheatsheets/
    * https://github.com/rstudio/cheatsheets

* Algunes en castellà:
    * https://www.rstudio.com/resources/cheatsheets/ > Spanish Translations – Traducciones en español

* Blogs, FAQs, Forums, ....
    * Blogs: 
        * RBloggers: https://www.r-bloggers.com/
    * Forums:
        * ADUP:  https://adup.cat/forum2
    * StackOverFlow
      https://stackoverflow.com/questions/tagged/r
      https://stackoverflow.com/questions/tagged/tidyverse
      * Altres: 
      https://appsilon.com/r-studio-shortcuts-and-tips/
* Google...

## Agraïments

* Eudald Correig Fraga - IISPV http://www.urv.cat/html/docencia-per-centre/general-17000285.php
* Companys del Departament de Gestió i Integració de dades de la OMD, AjBCN
* Desenvolupadors de [GNU/Linux](https://es.wikipedia.org/wiki/GNU/Linux), [Debian](https://ca.wikipedia.org/wiki/Debian), [Slax](https://www.slax.org/), [R](https://www.r-project.org/), [Rstudio](https://www.rstudio.com/), [Git](https://ca.wikipedia.org/wiki/Git), ...
* Richard M. Stallman - [Freee Software Foundation](https://fsf.org), per iniciar el moviment i filosofia del programari lliure
