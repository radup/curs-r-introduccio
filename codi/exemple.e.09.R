# --- llibreries -----------------------------------------------#
p_load(fs)
p_load(rgdal)
p_load(maptools)
p_load(gpclib)
if(!gpclibPermit()) print("cal instal·lar la versió correcta de Rtools: https://cran.r-project.org/bin/windows/Rtools/")

# getwd()
setwd("codi")
# Descomprimim arxiu descarregat de CartoBCN. 
unzip("../dades/e/CartoBCN-20190419_DIVADM.zip", exdir="../dades/e/")
# Descomprimim ara el zip intern que ha sortit de l'anterior 
unzip("../dades/e/CAD_CIUTAT/DIVADM/BCN_DIVADM_ETRS89_SHP.ZIP", exdir="../dades/e/SHP/")
fs::dir_info("../dades/e/SHP/")[1:10]

# Compte que els arxius des de l'11 en endavant tenen accents que la funció unzip de l'R no ha sabut reconeixer adequadament, al menys en el meu cas. Sembla que podrien ser en conjunt de caracters Win1252 i KDE del Kubuntu no hi treballa bé per mostrar o esborrar aquests altres arxius. Per explorar el tema de conjunts de caracters, descomenta les següents 2 línies:
#p_load(stringi, fs)
#stringi::stri_enc_detect(fs::dir_ls("../dades/e/SHP/")[11], filter_angle_brackets = FALSE)

#---------------------------------------------------------------------
# Informació derivada a partir d'script base de Marc Adzerias Forn. OMD-GID. AjBCN
#---------------------------------------------------------------------

# Es poden agafar els SHP de:
#  K:\QUOTA\OMD\INTEGRACIO\dades\BasesGrafiques\Shp Divisions Administratives a l'OMD 
# o de
#  CartoBCN: https://w20.bcn.cat/CartoBCN/

#K:\QUOTA\OMD\INTEGRACIO\dades\BasesGrafiques\Shp Divisions Administratives

#shp.path <- file.path("K:","QUOTA","OMD","INTEGRACIO","dades","BasesGrafiques","Shp Divisions Administratives","BCN_Districte_ETRS89_SHP.shp")
shp.path <- file.path("../dades/e/SHP/BCN_Districte_ETRS89_SHP.shp")


# --- lectura de shp -------------------------------------------#
# nota: mapa.DTE -> objecte classe SpatialPolygonsDataFrame
mapa.DTE <- readOGR(dsn = shp.path, encoding = "UTF-8")  # llegim fitxer .shp

# nota: mapa.DTE -> objecte classe SpatialPolygonsDataFrame
# 2 parts -> vectors amb dades referides als poligons i poligons
mapa.DTE$nou.atribut <- c(1001,1002,1003,1004,1005,1006,1007,1008,1009,1010)

plot(mapa.DTE)
text(x=mapa.DTE$Coord_X, y=mapa.DTE$Coord_Y, labels = mapa.DTE$nou.atribut)
